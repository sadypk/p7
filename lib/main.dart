import 'package:flutter/material.dart';
import 'package:p7tasks/p7_UI/Login_UI.dart';
import 'package:p7tasks/p7_UI/Register_UI.dart';
import 'package:p7tasks/p7_UI/SearchJob_UI.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(

      ),
      home: Registration_UI(),
    );
  }
}
