import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/fa_icon.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SearchJob extends StatefulWidget {
  @override
  _SearchJobState createState() => _SearchJobState();
}

class _SearchJobState extends State<SearchJob> {
  final sort = ['Most Recent', 'Deadline Available'];
  String selectedSort = 'Most Recent';
  bool heart;

  @override
  void initState() {
    // TODO: implement initState
    heart = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        height: height,
        width: width,
        color: Colors.white10,
        child: ListView(
          children: <Widget>[

            //Search Bar
            Container(
              margin: EdgeInsets.only(left: 15, right: 15, top: 10),
              width: width-30,
              child: Stack(
                children: <Widget>[
                  TextFormField(
                    decoration: InputDecoration(
                        hintText: 'Enter Keywords'
                    ),
                  ),
                  Positioned(
                    right: 5,
                    top: 10,
                    child: Icon(Icons.search, color: Colors.grey,size: 30,),
                  )
                ],
              )
            ),

            //The white section
            Container(
              margin: EdgeInsets.only(left: 15, right: 15, top: 70),
              padding: EdgeInsets.only(bottom: 30),
              width: width-30,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey[200],
                    offset: Offset(1,1),
                    spreadRadius: 1,
                  ),
                  BoxShadow(
                    color: Colors.grey[100],
                    offset: Offset(-1,-1),
                    spreadRadius: 1,
                  ),
                ]
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[

                  //Sort Drop Down
                  Container(
                    height: 60,
                    width: 185,
                    margin: EdgeInsets.only(top: 30, left: 20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.grey[200]
                    ),
                    child: Padding(
                      padding: EdgeInsets.only(left: 15,top: 5,right: 15),
                      child: DropdownButtonFormField<String>(
                        decoration: InputDecoration.collapsed(hintText: null),
                        items: sort.map((String value){
                          return DropdownMenuItem(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        value: selectedSort,
                        onChanged: (String value){
                          onDropDownChanged(value);
                        },
                      ),
                    ),
                  ),


                  //Job post
                  Container(
                    margin: EdgeInsets.only(left: 25, top: 50),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[

                        //That pointless fruit logo
                        Container(
                          height: 100,
                          width: 100,
                          margin: EdgeInsets.only(bottom: 25),
                          color: Colors.grey[100],
                          child: Center(child: FaIcon(FontAwesomeIcons.atom, size: 50,color: Colors.purpleAccent,),),
                        ),

                        //Job Title
                        Text('Web Developer', style: TextStyle(color: Colors.blue, fontSize: 25),),
                        Container(
                          margin: EdgeInsets.only(top: 25),
                          child: Row(
                            children: <Widget>[

                              //Company Name & Icon
                              Container(
                                margin: EdgeInsets.only(right: 40),
                                child: Row(
                                  children: <Widget>[
                                    FaIcon(FontAwesomeIcons.briefcase, color: Colors.grey,size: 20,),
                                    Container(margin: EdgeInsets.only(left: 5),child: Text('Ishraak Solutions', style: TextStyle(color: Colors.grey),))
                                  ],
                                ),
                              ),

                              //Location Name & Icon
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    Icon(Icons.location_on, color: Colors.grey,),
                                    Text('Dhaka', style: TextStyle(color: Colors.grey),)
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),

                        //Job time
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.access_time, color: Colors.red,),
                              Text('Full Time', style: TextStyle(color: Colors.grey),)
                            ],
                          ),
                        ),

                        //Apply Button
                        Container(
                          margin: EdgeInsets.only(top: 30, bottom: 20),
                          child: Row(
                            children: <Widget>[
                              Container(
                                height: 50,
                                width: 100,
                                margin: EdgeInsets.only(right: 30),
                                decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Center(
                                  child: Text(
                                    'Apply', style: TextStyle(fontSize: 20, color: Colors.grey[700]),
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: (){
                                  if(heart == false){
                                    heart = true;
                                  }else{heart = false;}
                                  setState(() {

                                  });
                                },
                                child: Container(
                                  height: 55,
                                  width: 55,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.grey[100],),
                                  child: Center(child: FaIcon(FontAwesomeIcons.heart,
                                    color: heart == true? Colors.redAccent: Colors.grey)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Text('Deadline: 2020-03-18', style: TextStyle(color: Colors.grey),),
                      ],
                    ),
                  )
                ],
              )
            )
          ],
        ),
      ),
    );
  }

  onDropDownChanged(String value){
    setState(() {
      this.selectedSort = value;
    });
  }
}


