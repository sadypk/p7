import 'package:flutter/material.dart';

class LoginUI extends StatefulWidget {
  @override
  _LoginUIState createState() => _LoginUIState();
}

class _LoginUIState extends State<LoginUI> {
  bool _checkboxValue = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: ListView(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 35,bottom: 8, left: 15, right: 15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 60,
                        width: 150,
                        child: Image.asset('assets/job-search-logo.png'),
                      ),

                      //Sign In TextField
                      Container(
                        width: MediaQuery.of(context).size.width - 30,
                        height: 50,
                        margin: EdgeInsets.only(top: 80),
                        color: Colors.grey[200],
                        child: Row(
                          children: <Widget>[
                            Container(
                              height: 50,
                              width: 50,
                              child: Icon(Icons.person, color: Colors.black54,),
                            ),
                            Container(
                              height: 50,
                              padding: EdgeInsets.only(top: 15),
                              width: MediaQuery.of(context).size.width - 80,
                              child: Text('Sign In',style: TextStyle(color: Colors.black54, fontWeight: FontWeight.bold, fontSize: 17),)
                            ),
                          ],
                        ),
                      ),

                      //Email Address TextField
                      Container(
                          width: MediaQuery.of(context).size.width - 30,
                          margin: EdgeInsets.only(top: 50),
                          child: Stack(
                            children: <Widget>[
                              TextFormField(
                                decoration: InputDecoration(
                                    hintText: 'Email Address',
                                    hintStyle: TextStyle(color: Colors.grey)
                                ),
                              ),
                              Positioned(
                                right: 5,
                                top: 10,
                                child: Icon(Icons.email, color: Colors.grey,),
                              )
                            ],
                          )
                      ),

                      //Password TextField
                      Container(
                          width: MediaQuery.of(context).size.width - 30,
                          margin: EdgeInsets.only(top: 50),
                          child: Stack(
                            children: <Widget>[
                              TextFormField(
                                decoration: InputDecoration(
                                    hintText: 'Password',
                                    hintStyle: TextStyle(color: Colors.grey)
                                ),
                              ),
                              Positioned(
                                right: 5,
                                top: 10,
                                child: Icon(Icons.lock, color: Colors.grey,),
                              )
                            ],
                          )
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 15,top: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Checkbox(
                            value: _checkboxValue,
                            checkColor: Colors.blue,
                            activeColor: Colors.white,
                            onChanged: (bool something){
                              _checkboxValue = something;
                              setState(() {

                              });
                            },
                          ),
                          Text('Remember Me', style: TextStyle(color: Colors.grey),),
                        ],
                      ),
                      Text('Forgot Password?', style: TextStyle(color: Colors.grey),),
                    ],
                  ),
                ),
                Center(
                  child: Container(
                    height: 60,
                    width: MediaQuery.of(context).size.width-30,
                    margin: EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.blue,
                    ),
                    child: Center(
                      child: Text('Sign In', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15),),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15,top: 20),
                  child: RichText(
                    text: TextSpan(
                      children: <TextSpan>[
                        TextSpan(text: 'Don\'t have an account?', style: TextStyle(color: Colors.grey, fontSize: 15)),
                        TextSpan(text: '  Register', style: TextStyle(color: Colors.lightBlueAccent, fontWeight: FontWeight.bold,fontSize: 15)),
                      ]
                    ),
                  ),
                )
              ],
            ),
          ],
        )
      ),
    );
  }
}
