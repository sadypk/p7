import 'package:flutter/material.dart';

class Registration_UI extends StatefulWidget {
  @override
  _Registration_UIState createState() => _Registration_UIState();
}

class _Registration_UIState extends State<Registration_UI> {
  bool _checkboxValue = false;
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
          height: height,
          width: width,
          padding: EdgeInsets.only(bottom: 20),
          child: ListView(
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 35,bottom: 8, left: 15, right: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: 60,
                          width: 150,
                          child: Image.asset('assets/job-search-logo.png'),
                        ),

                        //Sign In Header
                        Container(
                          width: MediaQuery.of(context).size.width - 30,
                          height: 50,
                          margin: EdgeInsets.only(top: 80),
                          color: Colors.grey[200],
                          child: Row(
                            children: <Widget>[
                              Container(
                                height: 50,
                                width: 50,
                                child: Icon(Icons.edit, color: Colors.black54,),
                              ),
                              Container(
                                  height: 50,
                                  padding: EdgeInsets.only(top: 15),
                                  width: width - 80,
                                  child: Text('Register Account',style: TextStyle(color: Colors.black54, fontWeight: FontWeight.bold, fontSize: 17),)
                              ),
                            ],
                          ),
                        ),

                        //Name TextField
                        Container(
                            width: MediaQuery.of(context).size.width - 30,
                            margin: EdgeInsets.only(top: 50),
                            child: Stack(
                              children: <Widget>[
                                TextFormField(
                                  decoration: InputDecoration(
                                      hintText: 'Name',
                                      hintStyle: TextStyle(color: Colors.grey)
                                  ),
                                ),
                                Positioned(
                                  right: 5,
                                  top: 10,
                                  child: Icon(Icons.text_format, color: Colors.grey,),
                                )
                              ],
                            )
                        ),

                        //Email
                        Container(
                            width: MediaQuery.of(context).size.width - 30,
                            margin: EdgeInsets.only(top: 50),
                            child: Stack(
                              children: <Widget>[
                                TextFormField(
                                  decoration: InputDecoration(
                                      hintText: 'Email',
                                      hintStyle: TextStyle(color: Colors.grey)
                                  ),
                                ),
                                Positioned(
                                  right: 5,
                                  top: 10,
                                  child: Icon(Icons.email, color: Colors.grey,),
                                )
                              ],
                            )
                        ),

                        //Mobile TextField
                        Container(
                            width: MediaQuery.of(context).size.width - 30,
                            margin: EdgeInsets.only(top: 50),
                            child: Stack(
                              children: <Widget>[
                                TextFormField(
                                  decoration: InputDecoration(
                                      hintText: 'Mobile',
                                      hintStyle: TextStyle(color: Colors.grey)
                                  ),
                                ),
                                Positioned(
                                  right: 5,
                                  top: 10,
                                  child: Icon(Icons.phone_android, color: Colors.grey,),
                                )
                              ],
                            )
                        ),

                        //Password TextField
                        Container(
                            width: MediaQuery.of(context).size.width - 30,
                            margin: EdgeInsets.only(top: 50),
                            child: Stack(
                              children: <Widget>[
                                TextFormField(
                                  decoration: InputDecoration(
                                      hintText: 'Password',
                                      hintStyle: TextStyle(color: Colors.grey)
                                  ),
                                ),
                                Positioned(
                                  right: 5,
                                  top: 10,
                                  child: Icon(Icons.lock, color: Colors.grey,),
                                )
                              ],
                            )
                        ),

                        //Confirm Password TextField
                        Container(
                            width: MediaQuery.of(context).size.width - 30,
                            margin: EdgeInsets.only(top: 50),
                            child: Stack(
                              children: <Widget>[
                                TextFormField(
                                  decoration: InputDecoration(
                                      hintText: 'Confirm Password',
                                      hintStyle: TextStyle(color: Colors.grey)
                                  ),
                                ),
                                Positioned(
                                  right: 0,
                                  top: 10,
                                  child: Icon(Icons.lock, color: Colors.grey[400],),
                                ),
                                Positioned(
                                  right: 5,
                                  top: 10,
                                  child: Icon(Icons.lock, color: Colors.grey[600],),
                                )
                              ],
                            )
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 15,top: 10),
                    child: Row(
                      children: <Widget>[
                        Checkbox(
                          value: _checkboxValue,
                          checkColor: Colors.blue,
                          activeColor: Colors.white,
                          onChanged: (bool something){
                            _checkboxValue = something;
                            setState(() {

                            });
                          },
                        ),
                        Text('I accept the terms & conditions', style: TextStyle(color: Colors.grey),),
                      ],
                    ),
                  ),
                  Center(
                    child: Container(
                      height: 60,
                      width: MediaQuery.of(context).size.width-30,
                      margin: EdgeInsets.only(top: 20),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Colors.blue,
                      ),
                      child: Center(
                        child: Text('Register', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15),),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15,top: 20),
                    child: RichText(
                      text: TextSpan(
                          children: <TextSpan>[
                            TextSpan(text: 'Already have an account?', style: TextStyle(color: Colors.grey, fontSize: 15)),
                            TextSpan(text: '  Sign In', style: TextStyle(color: Colors.lightBlueAccent, fontWeight: FontWeight.bold,fontSize: 15)),
                          ]
                      ),
                    ),
                  )
                ],
              ),
            ],
          )
      ),
    );
  }
}
